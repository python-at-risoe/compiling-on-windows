.. _errors:

=================================
Common issues and error messages
=================================

Here is a list of the most common issues and error messages we have found
while struggling with compiling.


- ``numpy.core.multiarray failed to import``

  Check that you have ``numpy`` installed in your test environment.
  
- ``unable to find vcvarsall.bat`` or ``cannot find -lmsvcr140``

  This might be caused by one of the following:
  
     a) you don't have the correct C++ redistributable installed, or  
     b) you need to install ``libpython`` and ``m2w64-toolchain`` into your
        Anaconda environment.

- ``ImportError: DLL load failed: The specified module could not be found``

  Double-check that you have created in your current environment the ``.cfg``
  file that tells Anaconda how to compile code ::

    (echo [build] & echo compiler = mingw32) > %CONDA_PREFIX%\Lib\distutils\distutils.cfg