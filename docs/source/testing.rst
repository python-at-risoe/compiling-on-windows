.. _testing:

===========================
Testing your compilers
===========================

The instructions here assume you have Anaconda installed. Note that you must run
``test_helloworlds.py`` in an environment with Python 3.6+. 

Here are the steps to test your compiler configuration to use ``cython`` and
``f2py`` with Anaconda:  

#. First make sure you have completed the compiler-configuration instructions:
   :ref:`configuration`.  

#. If you haven't already, clone the GitLab repository to your local machine
   so you have the code and then ``cd`` into it::

    git clone https://gitlab.windenergy.dtu.dk/python-at-risoe/compiling-on-windows.git
    cd compiling-on-windows

#. If you created a separated environment for your ``f2py`` and ``cython``
   setup in the configuration steps and it's not currently active, activate
   it::

    activate py36_test

#. Compile the provided ``.f90`` file (with ``f2py``) and ``.pyx`` file (with
   ``cython``) either manually using the terminal or automatically by running
   the provided bat file from an Anaconda prompt::
   
     compile_helloworlds.bat

   If you want to compile from the terminal, you can find the commands either
   in the bat file or by reading more here: :ref:`notes`.
 
#. Test that everything compiled correctly by running the provided test script
   from your Anaconda prompt::

     python test_helloworlds.py
     
If your compilers are configured correctly, the output should look like this:

::

  Hello, World! I am Fortran!

  Hello, World! I am Cython!

 Test complete! 0 modules were not found.
