.. Documentation master file for Compiling on Windows.

How To: Compile on Windows with Anaconda
========================================

Welcome to our instructions on configuring free compilers to successfully work
with ``f2py`` and ``cython`` on Windows with Anaconda.

How to use this repo
--------------------

This repo will walk you through the steps to compile two helloworld examples,
one with ``f2py`` and one with ``cython``. There are a few different pages here
that may be useful to you:

- :ref:`configuration`: instructions on installing the compilers and
  configuring Anaconda to work with them
- :ref:`testing`: compiling the helloworld examples to check your compilers are
  configured correctly
- :ref:`errors`: our list of common issues we run into when configuring
  compilers
- :ref:`notes`: some random notes on ``cython`` and ``f2py``

If you think something is missing or the instructions are unclear, please be so
kind as to `submit an issue on GitLab <https://gitlab.windenergy.dtu.dk/python-at-risoe/compiling-on-windows/issues>`__
so we can update our instructions.

Table of contents
-------------------

.. toctree::
    :maxdepth: 2

    configuration
    testing
    common_errors
    notes
