.. _configuration:

===========================
Configuring your compilers
===========================

We use the MinGW-w64 compiler, which is a free 64-bit compiler for Windows.
These instructions are basically a direct copy of those created by
`Michael Hirsch, Ph.D. <https://www.scivision.co/f2py-running-fortran-code-in-python-on-windows/>`__,
with a few extra steps added thanks to advice from
`mingwandroid on GitHub <https://github.com/ContinuumIO/anaconda-issues/issues/2449>`__
on help with final configuration issues. We very gratefully acknowledge these people
who helped us figure out how to repeatably configure our Windows compilers.

We recommend creating a new environment for these instructions to prevent
conflicting dependencies. Once you've verified that the procedure works in a
clean environment, then you can repeat the process in another environment where
things are already installed. Lastly, please note that any pre-existing fortran
compilers and PATH additions may cause these instructions to not work.


To set up your compilers, follow these steps:

#. Download and install `Anaconda <https://conda.io/docs/user-guide/install/download.html>`__.

#. Download the MinGW-w64 installer from `Source Forge <https://sourceforge.net/projects/mingw-w64/>`__.

#. Install the MinGW compiler with the following options:  

   - Architecture: ``x86_64``
   - Threads: ``posix``
   - Exception: ``seh``
   - Build revision: ``0``
   - Destination folder: ``c:\mingw``

#. Insert the path to the mingw bin folder to your system path. This will
   differ depending on whether you're running Windows 7 or 10, so we recommend
   googling how to add something to your path variable for your architecture.
   The path that you want to add to your system variable should look something
   like ``C:\mingw\mingw64\bin``.

#. [*Optional but recommended*] In an Anaconda prompt, create the environment
   in which you want to have ``f2py`` and ``cython`` working, and then activate
   it::  

     conda create -n py36_test python=3.6
     activate py36_test

#. Configure your Anaconda environment to use MinGW when it compiles by 
   executing the command below from the Anaconda prompt.  
   
   **IMPORTANT!!!** You must execute this command in every environment you
   want to compile from!  
   ::  

     (echo [build] & echo compiler = mingw32) > %CONDA_PREFIX%\Lib\distutils\distutils.cfg

#. Install several necessary packages to your environment::  

     conda install numpy libpython m2w64-toolchain cython

#. Make sure you have the correct C++ redistributable installed on your
   computer. The type you need will depend on what version was used to build
   Anaconda. Detailed instructions on this can be found
   `here <https://wiki.python.org/moin/WindowsCompilers>`__.

Your system should now be configured to compile Fortran code and to cythonize
Python code. You can test your configuration by going through the instructions
here: :ref:`testing`.
