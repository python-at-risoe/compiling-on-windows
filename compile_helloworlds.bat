@echo off
cd src
cythonize -a -i helloworld_cython.pyx
f2py -c -m helloworld_fortran helloworld_fortran.f90
cd ..