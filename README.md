# Notes on running f2py and Cython on Windows

Windows is not very fun for setting up compilers and things when you are
trying to stay with free tools. We've had a lot of trouble getting f2py and
cython up and running on my machine, so we're trying to make notes to make this
easier to repeat next time with someone else.

Detailed instructions are now here: https://python-at-risoe.pages.windenergy.dtu.dk/compiling-on-windows
