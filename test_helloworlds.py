"""Test that the cython code was correctly compiled
"""

# protect this code from running on import
if __name__ == '__main__':

    mods_skipped = 0
    try:  # try to load the fortran-compiled module
        import src.helloworld_fortran as hwf
        hwf.fortran_greetings()  # test the fortran function
    except ModuleNotFoundError:
        mods_skipped += 1
        pass

    try:  # try to load the cython-compiled module
        import src.helloworld_cython as hwc
        hwc.cython_greetings()  # test the cython function
    except ModuleNotFoundError:
        mods_skipped += 1
        pass

    print(f'\nTest complete! {mods_skipped} modules were not found.')
